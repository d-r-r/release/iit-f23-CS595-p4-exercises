#include <core.p4>
#include <xsa.p4>

// ****************************************************************************** //
// *************************** H E A D E R S  *********************************** //
// ****************************************************************************** //

// Define the Ethernet header format.
header ethernet_t {
    bit<48> dstAddr;      // Destination MAC address.
    bit<48> srcAddr;      // Source MAC address.
    bit<16> etherType;    // Ethernet type.
}

// ****************************************************************************** //
// ************************* S T R U C T U R E S  ******************************* //
// ****************************************************************************** //

// Define header structures.
struct headers {
    ethernet_t ethernet;  // Ethernet header.
}

// Define metadata structure for SmartNIC processing.
struct smartnic_metadata {
    bit<64> timestamp_ns;    // 64b timestamp (nanoseconds). Set when the packet arrives.
    bit<16> pid;             // 16b packet id for platform (READ ONLY - DO NOT EDIT).
    bit<3>  ingress_port;    // 3b ingress port (0:CMAC0, 1:CMAC1, 2:HOST0, 3:HOST1).
    bit<3>  egress_port;     // 3b egress port (0:CMAC0, 1:CMAC1, 2:HOST0, 3:HOST1).
    bit<1>  truncate_enable; // Reserved (tied to 0).
    bit<16> truncate_length; // Reserved (tied to 0).
    bit<1>  rss_enable;      // Reserved (tied to 0).
    bit<12> rss_entropy;     // Reserved (tied to 0).
    bit<4>  drop_reason;     // Reserved (tied to 0).
    bit<32> scratch;         // Reserved (tied to 0).
}

// ****************************************************************************** //
// *************************** P A R S E R  ************************************* //
// ****************************************************************************** //

// Define the parser state machine.
parser ParserImpl(packet_in packet,
                   out headers hdr,
                   inout smartnic_metadata sn_meta,
                   inout standard_metadata_t smeta) {
    state start {
        transition parse_ethernet;
    }

    // Parse Ethernet header.
    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition accept;
    }
}

// ****************************************************************************** //
// **************************  P R O C E S S I N G   **************************** //
// ****************************************************************************** //

// Define the processing logic for matching and actions.
control MatchActionImpl(inout headers hdr,
                         inout smartnic_metadata sn_meta,
                         inout standard_metadata_t smeta) {

    // Define action to forward the packet to a specified port.
    action forwardPacket(bit<3> dest_port) {
        sn_meta.egress_port = dest_port;  // Set egress port in metadata.
    }
    
    // Define action to drop the packet.
    action dropPacket() {
        smeta.drop = 1;  // Set drop flag in standard metadata.
    }

    // Define the forwarding table.
    table forward {
        key     = { hdr.ethernet.dstAddr : lpm; }  // Match on destination MAC address.
        actions = { forwardPacket; 
                    dropPacket;
                    NoAction; }  // Actions to perform for different matches.
        size    = 128;  // Table size.
        num_masks = 8;  // Number of masks supported.
        default_action = NoAction;  // Default action if no match is found.
    }

    apply {
        // Check for parser errors and drop the packet if there is an error.
        if (smeta.parser_error != error.NoError) {
            dropPacket();
            return;
        }
        
        // Check if Ethernet header is valid.
        if (hdr.ethernet.isValid()) {
            // Modify metadata for RSS and forwarding.
            sn_meta.rss_entropy = 9w0 ++ sn_meta.ingress_port;
            sn_meta.rss_enable = 1w1;
            forward.apply();  // Apply the forwarding table.
        }
        else
            dropPacket();  // Drop the packet if Ethernet header is invalid.
    }
}

// ****************************************************************************** //
// ***************************  D E P A R S E R  ******************************** //
// ****************************************************************************** //

// Define the deparser logic.
control DeparserImpl(packet_out packet,
                      in headers hdr,
                      inout smartnic_metadata sn_meta,
                      inout standard_metadata_t smeta) {
    apply {
        packet.emit(hdr.ethernet);  // Emit the Ethernet header.
    }
}

// ****************************************************************************** //
// *******************************  M A I N  ************************************ //
// ****************************************************************************** //

// Define the main pipeline.
XilinxPipeline(
    ParserImpl(), 
    MatchActionImpl(), 
    DeparserImpl()
) main;
