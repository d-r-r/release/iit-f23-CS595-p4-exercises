# P4 Tutorial for Xilinx p4c-vitisnet and ESnet SmartNIC Framework

Welcome to the P4 Tutorial for Xilinx p4c-vitisnet and ESnet SmartNIC Framework! This fork of the original P4 Tutorial has been adapted to be compliant with the Xilinx p4c-vitisnet framework and has been tested using the ESnet SmartNIC framework for Alveo FPGAs. In this tutorial, you will find a series of exercises to help you get started with P4 programming specifically tailored to these frameworks.

## Introduction

This tutorial includes a set of exercises organized into different modules, each focusing on specific aspects of P4 programming for Xilinx p4c-vitisnet and ESnet SmartNIC:

1. **Introduction and Language Basics**
   - [Loopback](./0-p4_only)
   - [Basic Forwarding](./1-basic_forwarding)
   - [Basic Tunneling](./2-basic_tunneling)

2. **Monitoring and Intermediate Examples**
   - [Explicit Congestion Notification](./3-ecn)
   - [Quality of Service](./4-qos)

3. **Advanced Behavior**
   - [Source Routing](./5-source_routing)
   - [Calculator](./6-calculator)


## P4 Documentation

You can refer to the following documentation for P4_16 and P4Runtime:
- [P4_16 and P4Runtime Documentation](https://p4.org/specs/)

# Original Tutorial

For the original tutorials and resources related to P4 programming, please refer to the [original P4 Tutorial](https://github.com/p4lang/tutorials) repository.