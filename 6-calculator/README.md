# Implementing a P4 Calculator

## Introduction

The objective of this tutorial is to implement a basic calculator
using a custom protocol header written in P4. The header will contain
an operation to perform and two operands. When a switch receives a
calculator packet header, it will execute the operation on the
operands, and return the result to the sender.am.


> **Spoiler alert:** A reference solution is available in the `solution` sub-directory for you to compare your implementation against.

## Step 1: Run the (incomplete) starter code

The directory containing this README also includes a skeleton P4 program named `program.p4`. Initially, this skeleton program drops all incoming packets. Your role is to extend this skeleton program to properly handle and forward IPv4 packets.

Before diving into the implementation, let's first compile the incomplete `program.p4` to verify its current behavior.

### A note about the control plane

It's essential to note that while a P4 program defines a packet-processing pipeline, the rules within each table are inserted by the control plane. When a rule matches an incoming packet, the associated action is invoked with parameters supplied by the control plane as specified within the rule.

## Step 2: Write your P4 logic

To implement the calculator, you will need to define a custom
calculator header, and implement the switch logic to parse header,
perform the requested operation, write the result in the header, and
return the packet to the sender.

We will use the following header format:

             0                1                  2              3
      +----------------+----------------+----------------+---------------+
      |      P         |       4        |     Version    |     Op        |
      +----------------+----------------+----------------+---------------+
      |                              Operand A                           |
      +----------------+----------------+----------------+---------------+
      |                              Operand B                           |
      +----------------+----------------+----------------+---------------+
      |                              Result                              |
      +----------------+----------------+----------------+---------------+


-  P is an ASCII Letter 'P' (0x50)
-  4 is an ASCII Letter '4' (0x34)
-  Version is currently 0.1 (0x01)
-  Op is an operation to Perform:
 -   '+' (0x2b) Result = OperandA + OperandB
 -   '-' (0x2d) Result = OperandA - OperandB
 -   '&' (0x26) Result = OperandA & OperandB
 -   '|' (0x7c) Result = OperandA | OperandB
 -   '^' (0x5e) Result = OperandA ^ OperandB


We will assume that the calculator header is carried over Ethernet,
and we will use the Ethernet type 0x1234 to indicate the presence of
the header.

Given what you have learned so far, your task is to implement the P4
calculator program. There is no control plane logic, so you need only
worry about the data plane implementation.

A working calculator implementation will parse the custom headers,
execute the mathematical operation, write the result in the result
field, and return the packet to the sender.

## Step 3: Run your solution

To test your implementation, follow these steps:

1. Source Vivado by running `source /tools/Xilinx/Vivado/2023.1/settings64.sh`.

2. Execute `make` to run all test cases. For each test case, you will find:
   - `packets_out.pcap`: Contains all the output packets.
   - `packets_out.meta`: Specifies the metadata for the output packets.

### Cleaning up

After completing the testing process, you can use `make clean` to remove the output generated from running the test cases.

### Known Issues

As of now, there are no known issues with this implementation. However, if you encounter any problems or have questions, please don't hesitate to reach out to us at [cs595-f2023-group@iit.edu](mailto:cs595-f2023-group@iit.edu). We are here to assist you with any challenges you may face during this exercise.
