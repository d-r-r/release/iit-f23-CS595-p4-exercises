# Implementing Basic Forwarding

## Introduction

The objective of this exercise is to develop a P4 program that implements basic forwarding for IPv4 packets. In this simple forwarding scheme, we will focus on three main actions for each packet: 

1. Update the source and destination MAC addresses.
2. Decrement the Time-to-Live (TTL) field in the IP header.
3. Forward the packet out through the appropriate output port.

For this project, your FPGA will contain a single table that the control plane will populate with static rules. Each rule will map an IP address to a corresponding MAC address and output port for the next hop in the network. We have already defined the control plane rules, so your primary task is to implement the data plane logic within your P4 program.

![Topology](topo.png)

> **Spoiler alert:** A reference solution is available in the `solution` sub-directory for you to compare your implementation against.

## Step 1: Run the (incomplete) starter code

The directory containing this README also includes a skeleton P4 program named `program.p4`. Initially, this skeleton program drops all incoming packets. Your role is to extend this skeleton program to properly handle and forward IPv4 packets.

Before diving into the implementation, let's first compile the incomplete `program.p4` to verify its current behavior.

### A note about the control plane

It's essential to note that while a P4 program defines a packet-processing pipeline, the rules within each table are inserted by the control plane. When a rule matches an incoming packet, the associated action is invoked with parameters supplied by the control plane as specified within the rule.

## Step 2: Write your P4 logic

In this step, you will write the P4 logic to implement basic forwarding for IPv4 packets. You will need to complete and extend the `program.p4` file to ensure that incoming packets are processed according to the specified actions.

## Step 3: Run your solution

To test your implementation, follow these steps:

1. Source Vivado by running `source /tools/Xilinx/Vivado/2023.1/settings64.sh`.

2. Execute `make` to run all test cases. For each test case, you will find:
   - `packets_out.pcap`: Contains all the output packets.
   - `packets_out.meta`: Specifies the metadata for the output packets.

### Cleaning up

After completing the testing process, you can use `make clean` to remove the output generated from running the test cases.

### Known Issues

As of now, there are no known issues with this implementation. However, if you encounter any problems or have questions, please don't hesitate to reach out to us at [cs595-f2023-group@iit.edu](mailto:cs595-f2023-group@iit.edu). We are here to assist you with any challenges you may face during this exercise.
