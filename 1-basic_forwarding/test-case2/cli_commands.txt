table_add forward forwardPacket 0x0a000101/32 => 0x080000000111 0x1 0

table_add forward forwardPacket 0x0a000202/32 => 0x080000000222 0x0 0

table_add forward forwardPacket 0x0a000303/32 => 0x080000000333 0x2 0

table_add forward forwardPacket 0x0a000404/32 => 0x080000000444 0x2 0

# run traffic
run_traffic packets

# end
exit
