#include <core.p4>
#include <xsa.p4>

// NOTE: new type added here
const bit<16> TYPE_MYTUNNEL = 0x1212;
const bit<16> TYPE_IPV4 = 0x800;

// ****************************************************************************** //
// *************************** H E A D E R S  *********************************** //
// ****************************************************************************** //

// Define the Ethernet header format.
header ethernet_t {
    bit<48> dstAddr;      // Destination MAC address.
    bit<48> srcAddr;      // Source MAC address.
    bit<16> etherType;    // Ethernet type.
}

// NOTE: added new header type
header myTunnel_t {
    bit<16> proto_id;
    bit<16> dst_id;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    bit<32>   srcAddr;
    bit<32>   dstAddr;
}

// ****************************************************************************** //
// ************************* S T R U C T U R E S  ******************************* //
// ****************************************************************************** //

// NOTE: Added new header type to headers struct
struct headers {
    ethernet_t   ethernet;
    myTunnel_t   myTunnel;
    ipv4_t       ipv4;
}

// Define metadata structure for SmartNIC processing.
struct smartnic_metadata {
    bit<64> timestamp_ns;    // 64b timestamp (nanoseconds). Set when the packet arrives.
    bit<16> pid;             // 16b packet id for platform (READ ONLY - DO NOT EDIT).
    bit<3>  ingress_port;    // 3b ingress port (0:CMAC0, 1:CMAC1, 2:HOST0, 3:HOST1).
    bit<3>  egress_port;     // 3b egress port (0:CMAC0, 1:CMAC1, 2:HOST0, 3:HOST1).
    bit<1>  truncate_enable; // Reserved (tied to 0).
    bit<16> truncate_length; // Reserved (tied to 0).
    bit<1>  rss_enable;      // Reserved (tied to 0).
    bit<12> rss_entropy;     // Reserved (tied to 0).
    bit<4>  drop_reason;     // Reserved (tied to 0).
    bit<32> scratch;         // Reserved (tied to 0).
}

// ****************************************************************************** //
// *************************** P A R S E R  ************************************* //
// ****************************************************************************** //

// TODO: Update the parser to parse the myTunnel header as well
parser ParserImpl(packet_in packet,
                   out headers hdr,
                   inout smartnic_metadata sn_meta,
                   inout standard_metadata_t smeta) {
    state start {
        transition parse_ethernet;
    }

    // Parse Ethernet header.
    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            default: accept;
        }
    }
    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition accept;
    }
}

// ****************************************************************************** //
// **************************  P R O C E S S I N G   **************************** //
// ****************************************************************************** //

// Define the processing logic for matching and actions.
control MatchActionImpl(inout headers hdr,
                         inout smartnic_metadata sn_meta,
                         inout standard_metadata_t smeta) {

    // Define action to forward the packet to a specified port.
    action forwardPacket(bit<48> dest_addr, bit<3> dest_port) {
        sn_meta.egress_port = dest_port;  // Set egress port in metadata.
        hdr.ethernet.srcAddr = hdr.ethernet.dstAddr;
        hdr.ethernet.dstAddr = dest_addr;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }
    
    // Define action to drop the packet.
    action dropPacket() {
        smeta.drop = 1;  // Set drop flag in standard metadata.
    }

    // Define the forwarding table.
    table forward {
        key = {
            hdr.ipv4.dstAddr: lpm;
        }
        actions = { forwardPacket; 
                    dropPacket;
                    NoAction; }  // Actions to perform for different matches.
        size    = 1024;  // Table size.
        default_action = dropPacket;  // Default action if no match is found.
    }



    // TODO: declare a new action: myTunnel_forward(egressSpec_t port)


    // TODO: declare a new table: myTunnel_exact
    // TODO: also remember to add table entries!



    apply {
        // Check for parser errors and drop the packet if there is an error.
        if (smeta.parser_error != error.NoError) {
            dropPacket();
            return;
        }
        

        
        // TODO: Update control flow
        if (hdr.ethernet.isValid()) {
            if (hdr.ipv4.isValid()){
                forward.apply();  // Apply the forwarding table.
            }
        }
        else
            dropPacket();  // Drop the packet if Ethernet header is invalid.
    }
}

// ****************************************************************************** //
// ***************************  D E P A R S E R  ******************************** //
// ****************************************************************************** //

// Define the deparser logic.
control DeparserImpl(packet_out packet,
                      in headers hdr,
                      inout smartnic_metadata sn_meta,
                      inout standard_metadata_t smeta) {
    apply {
        packet.emit(hdr.ethernet);  // Emit the Ethernet header.
        // TODO: emit myTunnel header as well
        packet.emit(hdr.ipv4);
    }
}

// ****************************************************************************** //
// *******************************  M A I N  ************************************ //
// ****************************************************************************** //

// Define the main pipeline.
XilinxPipeline(
    ParserImpl(), 
    MatchActionImpl(), 
    DeparserImpl()
) main;
