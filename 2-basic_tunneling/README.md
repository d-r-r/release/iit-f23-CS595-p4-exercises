# Implementing Basic Tunneling

## Introduction

In this exercise, we will add support for a basic tunneling protocol to the IP
router that you completed in the previous assignment.  The basic switch
forwards based on the destination IP address.  Your jobs is to define a new
header type to encapsulate the IP packet and modify the switch code, so that it
instead decides the destination port using a new tunnel header.

The new header type will contain a protocol ID, which indicates the type of
packet being encapsulated, along with a destination ID to be used for routing.

![Topology](topo.png)

> **Spoiler alert:** A reference solution is available in the `solution` sub-directory for you to compare your implementation against.

## Step 1: Run the (incomplete) starter code

The directory containing this README also includes a skeleton P4 program named `program.p4`. Initially, this skeleton program drops all incoming packets. Your role is to extend this skeleton program to properly handle and tunnel IPv4 packets.

Before diving into the implementation, let's first compile the incomplete `program.p4` to verify its current behavior.

### A note about the control plane

It's essential to note that while a P4 program defines a packet-processing pipeline, the rules within each table are inserted by the control plane. When a rule matches an incoming packet, the associated action is invoked with parameters supplied by the control plane as specified within the rule.

## Step 2: Write your P4 logic

Your job will be to do the following:

1. **NOTE:** A new header type has been added called `myTunnel_t` that contains
two 16-bit fields: `proto_id` and `dst_id`.
2. **NOTE:** The `myTunnel_t` header has been added to the `headers` struct.
2. **TODO:** Update the parser to extract either the `myTunnel` header or
`ipv4` header based on the `etherType` field in the Ethernet header. The
etherType corresponding to the myTunnel header is `0x1212`. The parser should
also extract the `ipv4` header after the `myTunnel` header if `proto_id` ==
`TYPE_IPV4` (i.e.  0x0800).
3. **TODO:** Define a new action called `myTunnel_forward` that simply sets the
egress port (i.e. `egress_spec` field of the `standard_metadata` bus) to the
port number provided by the control plane.
4. **TODO:** Define a new table called `myTunnel_exact` that perfoms an exact
match on the `dst_id` field of the `myTunnel` header. This table should invoke
either the `myTunnel_forward` action if the there is a match in the table and
it should invoke the `drop` action otherwise.
5. **TODO:** Update the `apply` statement in the `MyIngress` control block to
apply your newly defined `myTunnel_exact` table if the `myTunnel` header is
valid. Otherwise, invoke the `forward` table if the `ipv4` header is valid.
6. **TODO:** Update the deparser to emit the `ethernet`, then `myTunnel`, then
`ipv4` headers. Remember that the deparser will only emit a header if it is
valid. A header's implicit validity bit is set by the parser upon extraction.
So there is no need to check header validity here.

## Step 3: Run your solution

To test your implementation, follow these steps:

1. Source Vivado by running `source /tools/Xilinx/Vivado/2023.1/settings64.sh`.

2. Execute `make` to run all test cases. For each test case, you will find:
   - `packets_out.pcap`: Contains all the output packets.
   - `packets_out.meta`: Specifies the metadata for the output packets.

### Cleaning up

After completing the testing process, you can use `make clean` to remove the output generated from running the test cases.

### Known Issues

As of now, there are no known issues with this implementation. However, if you encounter any problems or have questions, please don't hesitate to reach out to us at [cs595-f2023-group@iit.edu](mailto:cs595-f2023-group@iit.edu). We are here to assist you with any challenges you may face during this exercise.
