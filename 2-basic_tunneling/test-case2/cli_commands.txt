table_add forward forwardPacket 0x0a000101/32 => 0x080000000111 0x0 0

table_add forward forwardPacket 0x0a000202/32 => 0x080000000222 0x1 0

table_add forward forwardPacket 0x0a000303/32 => 0x080000000333 0x1 0

table_add forward forwardPacket 0x0a000404/32 => 0x080000000444 0x1 0

table_add myTunnel_exact myTunnel_forward 0x0001 => 0x1

table_add myTunnel_exact myTunnel_forward 0x0002 => 0x0

table_add myTunnel_exact myTunnel_forward 0x0003 => 0x2

table_add myTunnel_exact myTunnel_forward 0x0004 => 0x2

# run traffic
run_traffic packets

# end
exit
