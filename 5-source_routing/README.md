# Implementing Source Routing

## Introduction

The objective of this exercise is to implement source routing.  With
source routing, the source host guides each switch in the network to
send the packet to a specific port. The host puts a stack of output
ports in the packet. In this example, we just put the stack after
Ethernet header and select a special etherType to indicate that.  Each
switch pops an item from the stack and forwards the packet according
to the specified port number.

Your switch must parse the source routing stack. Each item has a bos
(bottom of stack) bit and a port number. The bos bit is 1 only for the
last entry of stack.  Then at ingress, it should pop an entry from the
stack and set the egress port accordingly. The last hop may also
revert back the etherType to `TYPE_IPV4`.

> **Spoiler alert:** A reference solution is available in the `solution` sub-directory for you to compare your implementation against.

## Step 1: Run the (incomplete) starter code

The directory containing this README also includes a skeleton P4 program named `program.p4`. Initially, this skeleton program drops all incoming packets. Your role is to extend this skeleton program to properly handle and forward IPv4 packets.

Before diving into the implementation, let's first compile the incomplete `program.p4` to verify its current behavior.

### A note about the control plane

It's essential to note that while a P4 program defines a packet-processing pipeline, the rules within each table are inserted by the control plane. When a rule matches an incoming packet, the associated action is invoked with parameters supplied by the control plane as specified within the rule.

## Step 2: Write your P4 logic

The `program.p4` file contains a skeleton P4 program with key
pieces of logic replaced by `TODO` comments. These should guide your
implementation---replace each `TODO` with logic implementing the
missing piece.

A complete `program.p4` will contain the following components:

1. Header type definitions for Ethernet (`ethernet_t`) and IPv4
   (`ipv4_t`) and Source Route (`srcRoute_t`).
2. **TODO:** Parsers for Ethernet and Source Route that populate
   `ethernet` and `srcRoutes` fields.
3. An action to drop a packet, using `dropPacket()`.
4. **TODO:** An action (called `srcRoute_nhop`), which will:
	1. Set the egress port for the next hop.
	2. remove the first entry of srcRoutes **Note:** the method call pop_front is not allowed in Xilinx Architecture
5. A control with an `apply` block that:
    1. checks the existence of source routes.
    2. **TODO:** if statement to change etherent.etherType if it is the last hop
    3. **TODO:** call srcRoute_nhop action
6. A deparser that selects the order in which fields inserted into the outgoing
   packet.

## Step 3: Run your solution

To test your implementation, follow these steps:

1. Source Vivado by running `source /tools/Xilinx/Vivado/2023.1/settings64.sh`.

2. Execute `make` to run all test cases. For each test case, you will find:
   - `packets_out.pcap`: Contains all the output packets.
   - `packets_out.meta`: Specifies the metadata for the output packets.

### Cleaning up

After completing the testing process, you can use `make clean` to remove the output generated from running the test cases.

### Known Issues

As of now, there are no known issues with this implementation. However, if you encounter any problems or have questions, please don't hesitate to reach out to us at [cs595-f2023-group@iit.edu](mailto:cs595-f2023-group@iit.edu). We are here to assist you with any challenges you may face during this exercise.
