#include <core.p4>
#include <xsa.p4>

const bit<16> TYPE_IPV4 = 0x800;

/* IP protocols */
const bit<8> IP_PROTOCOLS_ICMP       =   1;
const bit<8> IP_PROTOCOLS_IGMP       =   2;
const bit<8> IP_PROTOCOLS_IPV4       =   4;
const bit<8> IP_PROTOCOLS_TCP        =   6;
const bit<8> IP_PROTOCOLS_UDP        =  17;
const bit<8> IP_PROTOCOLS_IPV6       =  41;
const bit<8> IP_PROTOCOLS_GRE        =  47;
const bit<8> IP_PROTOCOLS_IPSEC_ESP  =  50;
const bit<8> IP_PROTOCOLS_IPSEC_AH   =  51;
const bit<8> IP_PROTOCOLS_ICMPV6     =  58;
const bit<8> IP_PROTOCOLS_EIGRP      =  88;
const bit<8> IP_PROTOCOLS_OSPF       =  89;
const bit<8> IP_PROTOCOLS_PIM        = 103;
const bit<8> IP_PROTOCOLS_VRRP       = 112;

// ****************************************************************************** //
// *************************** H E A D E R S  *********************************** //
// ****************************************************************************** //

// Define the Ethernet header format.
header ethernet_t {
    bit<48> dstAddr;      // Destination MAC address.
    bit<48> srcAddr;      // Source MAC address.
    bit<16> etherType;    // Ethernet type.
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    bit<32>   srcAddr;
    bit<32>   dstAddr;
}

// ****************************************************************************** //
// ************************* S T R U C T U R E S  ******************************* //
// ****************************************************************************** //

// Define header structures.
struct headers {
    ethernet_t ethernet;  // Ethernet header.
    ipv4_t ipv4;
}

// Define metadata structure for SmartNIC processing.
struct smartnic_metadata {
    bit<64> timestamp_ns;    // 64b timestamp (nanoseconds). Set when the packet arrives.
    bit<16> pid;             // 16b packet id for platform (READ ONLY - DO NOT EDIT).
    bit<3>  ingress_port;    // 3b ingress port (0:CMAC0, 1:CMAC1, 2:HOST0, 3:HOST1).
    bit<3>  egress_port;     // 3b egress port (0:CMAC0, 1:CMAC1, 2:HOST0, 3:HOST1).
    bit<1>  truncate_enable; // Reserved (tied to 0).
    bit<16> truncate_length; // Reserved (tied to 0).
    bit<1>  rss_enable;      // Reserved (tied to 0).
    bit<12> rss_entropy;     // Reserved (tied to 0).
    bit<4>  drop_reason;     // Reserved (tied to 0).
    bit<32> scratch;         // Reserved (tied to 0).
}

// ****************************************************************************** //
// *************************** P A R S E R  ************************************* //
// ****************************************************************************** //

// Define the parser state machine.
parser ParserImpl(packet_in packet,
                   out headers hdr,
                   inout smartnic_metadata sn_meta,
                   inout standard_metadata_t smeta) {
    state start {
        transition parse_ethernet;
    }

    // Parse Ethernet header.
    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            default: accept;
        }
    }
    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition accept;
    }
}

// ****************************************************************************** //
// **************************  P R O C E S S I N G   **************************** //
// ****************************************************************************** //

// Define the processing logic for matching and actions.
control MatchActionImpl(inout headers hdr,
                         inout smartnic_metadata sn_meta,
                         inout standard_metadata_t smeta) {
    

    /* Default Forwarding */
    action default_forwarding() {
        hdr.ipv4.diffserv = 0;
    }

    /* Expedited Forwarding */
    action expedited_forwarding() {
        hdr.ipv4.diffserv = 46;
    }

    /* Voice Admit */
    action voice_admit() {
        hdr.ipv4.diffserv = 44;
    }

    /* Assured Forwarding */
    /* Class 1 Low drop probability */
    action af_11() {
        hdr.ipv4.diffserv = 10;
    }

    /* Class 1 Med drop probability */
    action af_12() {
        hdr.ipv4.diffserv = 12;
    }

    /* Class 1 High drop probability */
    action af_13() {
        hdr.ipv4.diffserv = 14;
    }

    /* Class 2 Low drop probability */
    action af_21() {
        hdr.ipv4.diffserv = 18;
    }

    /* Class 2 Med drop probability */
    action af_22() {
        hdr.ipv4.diffserv = 20;
    }

    /* Class 2 High drop probability */
    action af_23() {
        hdr.ipv4.diffserv = 22;
    }

    /* Class 3 Low drop probability */
    action af_31() {
        hdr.ipv4.diffserv = 26;
    }

    /* Class 3 Med drop probability */
    action af_32() {
        hdr.ipv4.diffserv = 28;
    }

    /* Class 3 High drop probability */
    action af_33() {
        hdr.ipv4.diffserv = 30;
    }

    /* Class 4 Low drop probability */
    action af_41() {
        hdr.ipv4.diffserv = 34;
    }

    /* Class 4 Med drop probability */
    action af_42() {
        hdr.ipv4.diffserv = 36;
    }

    /* Class 4 High drop probability */
    action af_43() {
        hdr.ipv4.diffserv = 38;
    }




    // Define action to forward the packet to a specified port.
    action forwardPacket(bit<48> dest_addr, bit<3> dest_port) {
        sn_meta.egress_port = dest_port;  // Set egress port in metadata.
        hdr.ethernet.srcAddr = hdr.ethernet.dstAddr;
        hdr.ethernet.dstAddr = dest_addr;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }
    
    // Define action to drop the packet.
    action dropPacket() {
        smeta.drop = 1;  // Set drop flag in standard metadata.
    }

    // Define the forwarding table.
    table forward {
        key = {
            hdr.ipv4.dstAddr: lpm;
        }
        actions = { forwardPacket; 
                    dropPacket;
                    NoAction; }  // Actions to perform for different matches.
        size    = 1024;  // Table size.
        default_action = NoAction;  // Default action if no match is found.
    }

    apply {
        // Check for parser errors and drop the packet if there is an error.
        if (smeta.parser_error != error.NoError) {
            dropPacket();
            return;
        }
        
        // Check if Ethernet header is valid.
        if (hdr.ethernet.isValid()) {
            if (hdr.ipv4.isValid()){
                if (hdr.ipv4.protocol == IP_PROTOCOLS_UDP) {
                    expedited_forwarding();
                }
                else if (hdr.ipv4.protocol == IP_PROTOCOLS_TCP) {
                    voice_admit();
                }
                forward.apply();  // Apply the forwarding table.
            }
        }
        else
            dropPacket();  // Drop the packet if Ethernet header is invalid.
    }
}

// ****************************************************************************** //
// ***************************  D E P A R S E R  ******************************** //
// ****************************************************************************** //

// Define the deparser logic.
control DeparserImpl(packet_out packet,
                      in headers hdr,
                      inout smartnic_metadata sn_meta,
                      inout standard_metadata_t smeta) {
    apply {
        packet.emit(hdr.ethernet);  // Emit the Ethernet header.
        packet.emit(hdr.ipv4);
    }
}

// ****************************************************************************** //
// *******************************  M A I N  ************************************ //
// ****************************************************************************** //

// Define the main pipeline.
XilinxPipeline(
    ParserImpl(), 
    MatchActionImpl(), 
    DeparserImpl()
) main;
