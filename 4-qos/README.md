# Implementing QOS

## Introduction

The objective of this tutorial is to extend basic L3 forwarding with
an implementation of Quality of Service (QOS) using Differentiated Services.

Diffserv is simple and scalable. It classifies and manages network traffic and provides QOS on modern IP networks.

As before, we have already defined the control plane rules for
routing, so you only need to implement the data plane logic of your P4
program.

> **Spoiler alert:** There is a reference solution in the `solution`
> sub-directory. Feel free to compare your implementation to the reference.

## Step 1: Run the (incomplete) starter code

The directory containing this README also includes a skeleton P4 program named `program.p4`. Initially, this skeleton program drops all incoming packets. Your role is to extend this skeleton program to properly handle and forward IPv4 packets.

Before diving into the implementation, let's first compile the incomplete `program.p4` to verify its current behavior.

### A note about the control plane

It's essential to note that while a P4 program defines a packet-processing pipeline, the rules within each table are inserted by the control plane. When a rule matches an incoming packet, the associated action is invoked with parameters supplied by the control plane as specified within the rule.

## Step 2: Write your P4 logic

The `program.p4` file contains a skeleton P4 program with key pieces of
logic replaced by `TODO` comments.  These should guide your
implementation---replace each `TODO` with logic implementing the
missing piece.

First we have to change the ipv4_t header by splitting the TOS field
into DiffServ and ECN fields.  Remember to update the checksum block
accordingly.  Then, in the egress control block we must compare the
protocol in IP header with IP protocols. Based on the traffic classes
and priority, the `diffserv` flag will be set.

A complete `program.p4` will contain the following components:

1. Header type definitions for Ethernet (`ethernet_t`) and IPv4 (`ipv4_t`).
2. Parsers for Ethernet, IPv4,
3. An action to drop a packet, using `dropPacket()`.
4. An action (called `fowardPacket`), which will:
	1. Set the egress port for the next hop.
	2. Update the ethernet destination address with the address of
           the next hop.
	3. Update the ethernet source address with the address of the switch.
	4. Decrement the TTL.
5. An ingress control block that checks the protocols and sets the ipv4.diffserv.
6. A deparser that selects the order in which headers are inserted into the outgoing
   packet.

## Step 3: Run your solution

To test your implementation, follow these steps:

1. Source Vivado by running `source /tools/Xilinx/Vivado/2023.1/settings64.sh`.

2. Execute `make` to run all test cases. For each test case, you will find:
   - `packets_out.pcap`: Contains all the output packets.
   - `packets_out.meta`: Specifies the metadata for the output packets.

### Cleaning up

After completing the testing process, you can use `make clean` to remove the output generated from running the test cases.

### Known Issues

As of now, there are no known issues with this implementation. However, if you encounter any problems or have questions, please don't hesitate to reach out to us at [cs595-f2023-group@iit.edu](mailto:cs595-f2023-group@iit.edu). We are here to assist you with any challenges you may face during this exercise.
