# Implementing ECN

## Introduction

The objective of this tutorial is to extend basic L3 forwarding with
an implementation of Explicit Congestion Notification (ECN).

ECN allows end-to-end notification of network congestion without
dropping packets.  If an end-host supports ECN, it puts the value of 1
or 2 in the `ipv4.ecn` field.  For such packets, each switch may
change the value to 3 if the queue size is larger than a threshold.
The receiver copies the value to sender, and the sender can lower the
rate.

As before, we have already defined the control plane rules for
routing, so you only need to implement the data plane logic of your P4
program.


![Topology](topo.png)

> **Spoiler alert:** A reference solution is available in the `solution` sub-directory for you to compare your implementation against.

## Step 1: Run the (incomplete) starter code

The directory containing this README also includes a skeleton P4 program named `program.p4`. Initially, this skeleton program drops all incoming packets. Your role is to extend this skeleton program to properly handle ecn in IPv4 packets.

Before diving into the implementation, let's first compile the incomplete `program.p4` to verify its current behavior.

### A note about the control plane

It's essential to note that while a P4 program defines a packet-processing pipeline, the rules within each table are inserted by the control plane. When a rule matches an incoming packet, the associated action is invoked with parameters supplied by the control plane as specified within the rule.

## Step 2: Write your P4 logic

First we have to change the ipv4_t header by splitting the TOS field
into DiffServ and ECN fields.  Remember to update the checksum block
accordingly.  Then, in the egress control block we must compare the
queue length with ECN_THRESHOLD. If the queue length is larger than
the threshold, the ECN flag will be set.  Note that this logic should
happen only if the end-host declared supporting ECN by setting the
original ECN to 1 or 2.

A complete `program.p4` will contain the following components:

1. Header type definitions for Ethernet (`ethernet_t`) and IPv4 (`ipv4_t`).
2. Parsers for Ethernet, IPv4,
3. An action to drop a packet, using `dropPacket()`.
4. An action (called `forwardPacket`), which will:
	1. Set the egress port for the next hop.
	2. Update the ethernet destination address with the address of
           the next hop.
	3. Update the ethernet source address with the address of the switch.
	4. Decrement the TTL.
5. An egress control block that checks the ECN and
`sn_meta.enq_qdepth` and sets the ipv4.ecn.
6. A deparser that selects the order in which fields inserted into the outgoing
   packet.


## Step 3: Run your solution

To test your implementation, follow these steps:

1. Source Vivado by running `source /tools/Xilinx/Vivado/2023.1/settings64.sh`.

2. Execute `make` to run all test cases. For each test case, you will find:
   - `packets_out.pcap`: Contains all the output packets.
   - `packets_out.meta`: Specifies the metadata for the output packets.

### Cleaning up

After completing the testing process, you can use `make clean` to remove the output generated from running the test cases.

### Known Issues

As of now, there are no known issues with this implementation. However, if you encounter any problems or have questions, please don't hesitate to reach out to us at [cs595-f2023-group@iit.edu](mailto:cs595-f2023-group@iit.edu). We are here to assist you with any challenges you may face during this exercise.
